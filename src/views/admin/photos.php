<?php

use maerduq\usmAlbums\assets\PhotoFormAsset;
use yii\helpers\Html;
use yii\helpers\Url;

PhotoFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\photos\models\Album */

$this->title = 'Update photos of album: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Photos';
?>

<div class='jumbotron'>
    <h3>Upload new images</h3>

    <form id="upload" method="post" action="<?= Url::to(['upload', 'id' => $model->id]) ?>" enctype="multipart/form-data">
        <input type="file" name="files[]" multiple>
        <small>Max file size: <?= ini_get("upload_max_filesize"); ?></small>
    </form>

    <br/>
    <span id="info"></span><span id="info2"></span><span id="info3"></span>
    <div class="progress" id='progress' style='display:none'>
        <div id='pg-success' class="progress-bar progress-bar-striped"></div>
        <div id='pg-failed' class="progress-bar progress-bar-danger progress-bar-striped"></div>
    </div>
    <?= Html::a("Reload", ['photos', 'id' => $model->id], ['id' => 'refresh-button', 'style' => 'display:none', 'class' => 'btn btn-primary btn-lg']) ?>
    <div class='alert alert-danger hidden' id='fails' style='margin-top:20px;margin-bottom:0'>
        <h4>Errors</h4>
        <ul id='fails-list'></ul>
    </div>
</div>
<h3>There are <b> <span id='count'><?= count($model->photos) ?></span> photos</b> in this album</h3>

<div class='list-group'>
    <?php foreach ($model->photos as $photo): ?>
        <div class='list-group-item'>
            <a href='<?= Url::to(['default/photo', 'id' => $model->id, 'file' => $photo, 'thumbnail' => 0]) ?>' data-lightbox='set' data-title='<?= $photo ?>'>
                <img src='<?= Url::to(['default/photo', 'id' => $model->id, 'file' => $photo]) ?>' style='margin: -10px 15px -10px -15px;height:80px'/>
            </a>
            <?= $photo ?>
            <div class='pull-right' style='line-height:60px'>
                <?php if ($photo == $model->cover_photo): ?>
                    <span class="badge">Cover photo</span>
                <?php else: ?>
                    <div class='actions'>
                        <?= Html::a('Delete', ['delete-photo', 'id' => $model->id, 'file' => $photo], ['class' => 'btn btn-danger button-delete']) ?>
                        <?= Html::a('Set as cover photo', ['set-cover-photo', 'id' => $model->id, 'file' => $photo], ['class' => 'btn btn-link']) ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    <?php endforeach ?>
</div>
<style>
    .list-group-item .actions {
        display: none;
    }

    .list-group-item:hover .actions {
        display: block;
    }
</style>