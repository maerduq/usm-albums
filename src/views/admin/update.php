<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\photos\models\Album */

$this->title = 'Update Album: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="album-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
