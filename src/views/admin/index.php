<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\photos\models\AlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Albums';
?>
<div class="album-index">
    <div class="content-toolbar">
        <?= Html::a('Create Album', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'date:date',
            'cover_photo',
            //            'created_at',
            // 'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {photos} {update} {delete}',
                'buttons' => [
                    'photos' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-picture"></span>', ['photos', 'id' => $model->id]);
                    },
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['default/album', 'id' => $model->id], ['target' => '_blank']);
                    },
                ]
            ],
        ],
    ]) ?>
</div>