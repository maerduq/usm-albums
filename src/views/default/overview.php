<?php

/* @var $this yii\web\View */
/* @var $albums maerduq\usmAlbums\models\Album[] */

$this->title = Yii::t('app', "Photo's");
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Url;
?>

<?php if (count($albums) === 0): ?>
    <div class="alert alert-info"><?= \maerduq\usm\models\Textblock::read('photos/no-photos-message') ?></div>
<?php else: ?>

    <div class="row">
        <?php foreach ($albums as $id => $album): ?>
            <div class="col-xs-6 col-md-4">
                <a href="<?= Url::to(['album', 'id' => $album->id]) ?>" class="thumbnail">
                    <div class="img" style="background-image:url('<?= Url::to(['photo', 'id' => $album->id, 'file' => $album->cover_photo]) ?>')"></div>
                    <div class="caption"><?= $album->screenName ?></div>
                </a>
            </div>
        <?php endforeach ?>
    </div>
<?php endif ?>

<style>
    .thumbnail .img {
        height: 180px;
        width: 100%;
        background-color: #ddd;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 50% 50%;
    }
</style>
