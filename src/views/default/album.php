<?php
/* @var $this yii\web\View */
/* @var $album maerduq\usmAlbums\models\Album */

use yii\helpers\Url;
use yii\helpers\Html;
use maerduq\usmAlbums\assets\LightboxAsset;

$this->title = $album->name;
$this->params['pageHeader'] = $this->title . '&nbsp;<small>' . $album->screenDate . '</small>';
$this->params['breadcrumbs'][] = ['label' => 'Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $album->name;

LightboxAsset::register($this);
?>

<?php if ($album->nextAlbum !== null || $album->previousAlbum !== null): ?>
    <div class='btn-toolbar'>
        <?php if ($album->nextAlbum !== null): ?>
            <?= Html::a('&larr; ' . $album->nextAlbum->screenName, ['album', 'id' => $album->nextAlbum->id], ['class' => 'btn btn-link']) ?>
        <?php endif ?>


        <?php if ($album->previousAlbum !== null): ?>
            <div class='pull-right'>
                <?= Html::a($album->previousAlbum->screenName . ' &rarr;', ['album', 'id' => $album->previousAlbum->id], ['class' => 'btn btn-link']) ?>
            </div>
        <?php endif ?>
    </div>
    <br/>
<?php endif ?>

<div class="row">
    <?php foreach ($album->photos as $id => $photo): ?>
        <div class="col-xs-6 col-md-3">
            <a href='<?= Url::to(['photo', 'id' => $album->id, 'file' => $photo, 'thumbnail' => 0]) ?>' class="thumbnail"  data-lightbox='set' data-title='<?= $photo ?>'>
                <div class="img" style="background-image:url('<?= Url::to(['photo', 'id' => $album->id, 'file' => $photo]) ?>')"></div>
            </a>
        </div>
    <?php endforeach ?>
</div>

<style>
    .thumbnail .img {
        height: 180px;
        width: 100%;
        background-color: #ddd;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 50% 50%;
    }
</style>
