<?php

namespace maerduq\usmAlbums;

use maerduq\usm\plugins\UsmPluginModule;

class AlbumsModule extends UsmPluginModule {
    public $controllerNamespace = 'maerduq\usmAlbums\controllers';
    public $imageDirectory = '@app/files/albums/';
    public $overviewViewFile = 'overview';
    public $albumViewFile = 'album';

    public function init() {
        parent::init();
    }

    public function getPluginPages() {
        return [
            [
                'name' => 'Album overview page',
                'actionId' => 'default/index',
            ],
        ];
    }

    public function getAdminMenuItems() {
        return [['icon' => 'images', 'label' => 'Albums', 'url' => ['admin/index']]];
    }

    public function getSitemapControllers() {
        return ['default'];
    }
}
