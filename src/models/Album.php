<?php

namespace maerduq\usmAlbums\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "photos_albums".
 *
 * @property integer $id
 * @property string $name
 * @property string $date
 * @property string $cover_photo
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $screenName
 */
class Album extends ActiveRecord {
    
    const DIR_THUMBNAILS = 'thumbnails';
    const DIR_PICTURES = 'pictures';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'photos_albums';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['date'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['name', 'cover_photo'], 'string', 'max' => 255]
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            foreach ([$this->albumPath(), $this->albumPath().self::DIR_THUMBNAILS, $this->albumPath().self::DIR_PICTURES] as $dir) {
                if (!file_exists($dir)) {
                    mkdir($dir);
                }
            }
        }
    }

    public function beforeDelete() {
        if (file_exists($this->albumPath())) {
            return self::_rrmdir($this->albumPath());
        }
        return true;
    }

    private static function _rrmdir($dir) {
        foreach (array_diff(scandir($dir), ['..', '.']) as $fileName) {
            $file = $dir . '/' . $fileName;
            if (is_dir($file)) {
                self::_rrmdir($file);
            } else {
                unlink($file);
            }
        }
        @unlink($dir);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date' => 'Date',
            'cover_photo' => 'Cover Photo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function getPreviousAlbum() {
        return static::find()->where("date < '{$this->date}' or (date = '{$this->date}' and id < {$this->id})")->orderBy(["DATEDIFF('{$this->date}', date)" => SORT_ASC, 'id' => SORT_DESC])->one();
    }
    
    public function getNextAlbum() {
        return static::find()->where("date > '{$this->date}' or (date = '{$this->date}' and id > {$this->id})")->orderBy(["DATEDIFF(date, '{$this->date}')" => SORT_ASC, 'id' => SORT_ASC])->one();
    }

    public function getPhotos() {
        return file_exists($this->albumPath()) ?
            array_diff(scandir($this->albumPath() . self::DIR_PICTURES), ['..', '.']) :
            [];
    }

    public function getYear() {
        return date('Y', strtotime($this->date));
    }

    public function getScreenName() {
        return $this->name . ' (' . date('d M', strtotime($this->date)) . ')';
    }

    public function getScreenDate() {
        $date = \DateTime::createFromFormat('Y-m-d', $this->date);
        return $date->format('d M \'y');
    }

    public function getDirectoryEmpty() {
        $dir = $this->albumPath();

        if (!is_readable($dir)) {
            return false;
        }
        $handle = opendir($dir . static::DIR_PICTURES);
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                return false;
            }
        }
        return true;
    }

    public function albumPath() {
        return static::albumsPath() . $this->id . '/';
    }

    public static function albumsPath() {
        $path = Yii::getAlias(Yii::$app->controller->module->imageDirectory);
        if (substr($path, -1) !== "/") {
            $path .= "/";
        }
        return $path;
    }

}
