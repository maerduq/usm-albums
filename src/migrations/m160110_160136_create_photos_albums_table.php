<?php

use yii\db\Migration;

class m160110_160136_create_photos_albums_table extends Migration {

    public function up() {
        $this->createTable('photos_albums', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'date' => $this->date(),
            'cover_photo' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()
        ]);
    }

    public function down() {
        $this->dropTable('photos_albums');
    }

}
