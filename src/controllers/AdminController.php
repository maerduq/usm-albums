<?php

namespace maerduq\usmAlbums\controllers;

use maerduq\usmAlbums\models\Album;
use maerduq\usmAlbums\models\AlbumSearch;
use maerduq\usm\components\UsmController;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AlbumController implements the CRUD actions for Album model.
 */
class AdminController extends UsmController {

    public function behaviors() {
        return parent::behaviors() + [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post', 'get'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $dir = Album::albumsPath();
        if (!file_exists($dir)) {
            throw new HttpException(400, 'Album directory (' . $dir . ') cannot be found');
        } elseif (!is_writable($dir)) {
            throw new HttpException(400, 'Album directory (' . $dir . ') is not writable');
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Album models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpload($id) {
        $model = $this->findModel($id);

        $imagine = new Imagine();
        $thumbnailSize = 300;
        $pictureSize = 2000;

        $files = UploadedFile::getInstancesByName('files');
        foreach ($files as $file) {
            if (!file_exists($file->tempName)) {
                throw new \yii\web\HttpException(400, "File could not be uploaded. Probably too big");
            }
            $file->name = basename($file->name);
            $imagine->open($file->tempName)->thumbnail(new Box(
                $thumbnailSize,
                $thumbnailSize
            ))->save($model->albumPath() . Album::DIR_THUMBNAILS . '/' . $file->name);
            $imagine->open($file->tempName)->thumbnail(new Box(
                $pictureSize,
                $pictureSize
            ))->save($model->albumPath() . Album::DIR_PICTURES . '/' . $file->name);

            if ($model->cover_photo === null) {
                $model->cover_photo = $file->name;
                $model->save();
            }
        }
        return '{}';
    }

    /**
     * Creates a new Album model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Album();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['photos', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Album model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionPhotos($id) {
        $model = $this->findModel($id);

        return $this->render('photos', [
            'model' => $model
        ]);
    }

    public function actionDeletePhoto($id, $file) {
        $model = $this->findModel($id);
        $file = basename($file);
        $unlink1 = unlink($model->albumPath() . Album::DIR_PICTURES . '/' . $file);
        $unlink2 = unlink($model->albumPath() . Album::DIR_THUMBNAILS . '/' . $file);
        $unlink = $unlink1 && $unlink2;

        if (!Yii::$app->request->isAjax) {
            if ($unlink) {
                Yii::$app->session->setFlash('success', 'Photo deleted!');
            } else {
                Yii::$app->session->setFlash('danger', 'Photo could not be deleted...');
            }
            $this->redirect(['photos', 'id' => $id]);
        } elseif (!$unlink) {
            throw new \yii\web\HttpException(400);
        }
    }

    public function actionSetCoverPhoto($id, $file) {
        $model = $this->findModel($id);
        $model->cover_photo = basename($file);
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Cover photo changed!');
        } else {
            Yii::$app->session->setFlash('danger', 'Cover photo could not be changed...');
        }

        $this->redirect(['photos', 'id' => $id]);
    }

    /**
     * Deletes an existing Album model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $force = false) {
        $album = $this->findModel($id);

        if ($album->directoryEmpty || $force) {
            $this->findModel($id)->delete();
        } else {
            Yii::$app->session->setFlash(
                'danger',
                'Album cannot be deleted, it is not empty. ' . Html::a(
                    'Delete album including all pictures? Click here!',
                    ['delete', 'id' => $id, 'force' => true],
                    ['class' => 'alert-link']
                )
            );
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Album model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Album the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Album::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
