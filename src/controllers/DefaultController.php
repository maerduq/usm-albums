<?php

namespace maerduq\usmAlbums\controllers;

use maerduq\usmAlbums\models\Album;
use yii\filters\AccessControl;

class DefaultController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex() {
        $albums = Album::find()->orderBy(['date' => SORT_DESC, 'id' => SORT_DESC])->all();

        return $this->render($this->module->overviewViewFile, [
            'albums' => $albums
        ]);
    }

    public function actionAlbum($id) {
        $album = Album::findOne($id);
        if ($album === null) {
            return $this->redirect(['index']);
        }

        return $this->render($this->module->albumViewFile, [
            'album' => $album
        ]);
    }

    public function actionPhoto($id, $file, $thumbnail = true) {
        $album = Album::findOne($id);
        $subDir = ($thumbnail) ? Album::DIR_THUMBNAILS : Album::DIR_PICTURES;

        $file = $album->albumPath() . $subDir . '/' . basename($file);

        header("Content-Type: " . \yii\helpers\FileHelper::getMimeTypeByExtension($file));
        flush();
        readfile($file);
        die();
    }

    public function sitemap() {
        return array_map(function ($item) {
            return ['url' => ['/photos/default/album', 'id' => $item->id]];
        }, Album::find()->all());
    }
}
