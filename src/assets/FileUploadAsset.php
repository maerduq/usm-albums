<?php
// documentation can be found on https://blueimp.github.io/jQuery-File-Upload/

namespace maerduq\usmAlbums\assets;

use yii\web\AssetBundle;

class FileUploadAsset extends AssetBundle {
    public $sourcePath = '@bower/blueimp-file-upload';
    public $js = [
        'js/vendor/jquery.ui.widget.js',
        'js/jquery.iframe-transport.js',
        'js/jquery.fileupload.js'
    ];
}
