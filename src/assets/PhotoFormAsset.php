<?php

namespace maerduq\usmAlbums\assets;

use yii\web\AssetBundle;

class PhotoFormAsset extends AssetBundle {
    public $sourcePath = '@vendor/maerduq/usm-albums/src/asset-files/photo-form';
    public $js = [
        'script.js'
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\maerduq\usmAlbums\assets\FileUploadAsset',
        '\maerduq\usmAlbums\assets\LightboxAsset',
    ];
    public $publishOptions = [
        'forceCopy' => true
    ];
}
