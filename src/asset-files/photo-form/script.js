function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }

    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }

    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }

    return (bytes / 1000).toFixed(2) + ' KB';
}
var files = 0;
var done = 0;
var failed = 0;

$(function() {
    $('#upload').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        add: function(e, data) {
            files++;
            $("#progress").show();
            $("#progress .progress-bar").addClass('active');
            $("#info").text("Uploading " + files + " file(s)");
            var jqXHR = data.submit();
        },
        progress: function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $("#progress #pg-success").css("width", parseInt(((done + progress / 100) / files) * 100, 10) + "%");
            $("#progress #pg-failed").css("width", parseInt((failed / files) * 100, 10) + "%");
        },
        done: function(e, data) {
            done++;
            updateProgress();
        },
        fail: function(e, data) {
            failed++;

            updateProgress();
            $("#fails").removeClass('hidden');
            $("#fails-list").append("<li><b>" + data.files[0].name + "</b>: " + data.jqXHR.responseText.split(": ")[1] + "</li>");
        }

    });
});

function updateProgress() {
    $("#info2").text(", " + done + " finished");
    $("#info3").text(", " + failed + " failed");

    $("#progress #pg-success").css("width", parseInt((done / files) * 100, 10) + "%");
    $("#progress #pg-failed").css("width", parseInt((failed / files) * 100, 10) + "%");
    if (done + failed == files) {
        $("#progress .progress-bar").removeClass('active');
        $("#refresh-button").fadeIn();
    }
}

$(document).ready(function() {
    $('.button-delete').click(function(event) {
        event.preventDefault();
        if (!confirm('Sure to delete this photo?')) {
            return false;
        }
        $.ajax({
            url: $(this).attr('href'),
            success: function() {
                $(this).closest('.list-group-item').fadeOut();
                $("#count").text(parseInt($("#count").text()) - 1);
            }.bind(this)
        });
        return false;
    });
});