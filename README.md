# USM Albums
A plugin for USM to manage and display photo's using albums.

## Prerequisites
* Composer using https://asset-packagist.org as repository.
* A project using [USM](https://bitbucket.org/maerduq/usm).

## Getting started
* Download in your project by executing `$ composer require maerduq/usm-albums`.
* Add the plugin as a module to `config/web.php`.
~~~
    'modules' => [
        ...
        'albums' => [
            'class' => 'maerduq\usmAlbums\AlbumsModule',
            (here some optional plugin configuration, see below for options)
        ],
        ...
    ],
    ...
~~~
* Add the albums module as USM plugin in `config/web.php`.
~~~
    'modules' => [
        'usm' => [
            ...
            'plugins' => [
                'albums',
                ...
            ],
        ],
        ...
    ],
    ...
~~~
* Add the migration path to `migrate-usm` in `config/console.php`.
~~~
    ...
    'controllerMap' => [
        'migrate-usm' => [
            'migrationPath' => ['@usm/migrations', '@vendor/maerduq/usm-albums/src/migrations'],
            ...
        ]
    ],
    ...
~~~
* Run the migrations with `$ php yii migrate-usm`.
* Create the folder `@app/files/albums` and make it writable, or configure a different folder to store your image files.
* Increase `post_max_size` and `upload_max_filesize` in your PHP configuration if needed.

## Hooks
* Controller to add to your menu: `/albums/default/index`.
* Textblocks: *photos/no-photos-message*.

## Plugin configuration
* `imageDirectory` - path to where the image files of the albums should be stored.
* `overviewViewFile` - view file for the overview view.
* `albumViewFile` - view file for the album view.
